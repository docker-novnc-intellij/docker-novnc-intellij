FROM  ubuntu:18.04
MAINTAINER Mark McCahill <mccahill@duke.edu>

ENV DEBIAN_FRONTEND noninteractive
ENV HOME /root

# use local repositories 
#RUN sed -i 's/archive.ubuntu.com/archive.linux.duke.edu/' /etc/apt/sources.list
#RUN sed -i 's/security.ubuntu.com/archive.linux.duke.edu/' /etc/apt/sources.list

RUN apt-get  update && apt-get -yq dist-upgrade \
 && apt-get  install -yq --no-install-recommends \
    wget \
    bzip2 \
    ca-certificates \
    apt-utils

RUN apt-get  update   &&  apt-get dist-upgrade -y

RUN apt-get  install -y --force-yes --no-install-recommends \
        python-numpy \ 
        software-properties-common \
        wget \
        curl \
        supervisor \
        openssh-server \
        pwgen \
        sudo \
        iputils-ping \
        vim-tiny \
        net-tools \
        lxde \
        lxde-common \
        menu \
        openbox \
        openbox-menu \
        xterm \
        obconf \
        obmenu \
        xfce4-terminal \
        python-xdg \
        scrot \
        x11vnc \
        xvfb \
        gtk2-engines-murrine \
        ttf-ubuntu-font-family \
        firefox \
        firefox-dev \
        libwebkitgtk-3.0-0 \
        xserver-xorg-video-dummy \
    && apt-get autoclean \
    && apt-get autoremove \
    && rm -rf /var/lib/apt/lists/*

#RUN snap install firefox

RUN mkdir /etc/startup.aux/
RUN echo "#Dummy" > /etc/startup.aux/00.sh
RUN chmod +x /etc/startup.aux/00.sh
RUN mkdir -p /etc/supervisor/conf.d
RUN rm /etc/supervisor/supervisord.conf

# create an ubuntu user
#PASS=`pwgen -c -n -1 10`
#PASS=ubuntu
#echo "User: ubuntu Pass: $PASS"
#RUN useradd --create-home --shell /bin/bash --user-group --groups adm,sudo ubuntu

# create an ubuntu user who cannot sudo
RUN useradd --create-home --shell /bin/bash --user-group ubuntu
RUN echo "ubuntu:badpassword" | chpasswd

ADD startup.sh /
ADD cleanup-cruft.sh /
ADD supervisord.conf.xorg /etc/supervisor/supervisord.conf
EXPOSE 6080

ADD openbox-config /openbox-config
RUN cp -r /openbox-config/.config ~ubuntu/
RUN chown -R ubuntu ~ubuntu/.config ; chgrp -R ubuntu ~ubuntu/.config
RUN rm -r /openbox-config

RUN echo 'rebuild from here'
# noVNC
ADD noVNC /noVNC/

# make sure the noVNC self.pem cert file is only readable by root
RUN chmod 400 /noVNC/self.pem

# store a password for the VNC service
RUN mkdir /home/root
RUN mkdir /home/root/.vnc
RUN x11vnc -storepasswd foobar /home/root/.vnc/passwd
ADD xorg.conf /etc/X11/xorg.conf


RUN apt-get update && apt-get install -yq \
 build-essential \
 libpng-dev \
 zlib1g-dev \
 libjpeg-dev \
 python-dev \
 imagemagick \
 python-pip \
 python-tk \
 python-numpy \
 python-scipy \
 python-matplotlib \
 git \
 python3-pip \
 python3-tk \
 python3-numpy \
 python3-scipy \
 python3-matplotlib

RUN  pip install pillow
RUN  pip3 install pillow


############ begin Liberica JDK version 12.0.2  ###############
# 
RUN wget https://download.bell-sw.com/java/12.0.2/bellsoft-jdk12.0.2-linux-amd64.deb
RUN dpkg -i bellsoft-jdk12.0.2-linux-amd64.deb
#
############ end Liberica JDK version 12.0.2 ###############

############ begin intellij ###############
# 
WORKDIR /opt/idea
RUN curl -fsSL https://download.jetbrains.com/idea/ideaIC-2019.2.1.tar.gz -o /opt/idea/installer.tgz \
    && tar --strip-components=1 -xzf installer.tgz \
    && rm installer.tgz
WORKDIR /
# 
############ end intellij ###############

# get rid of some LXDE & OpenBox cruft that doesn't work and clutters menus
RUN rm /usr/share/applications/display-im6.q16.desktop & \
    rm /usr/share/applications/display-im6.desktop & \
    rm /usr/share/applications/lxterminal.desktop & \
    rm /usr/share/applications/debian-uxterm.desktop & \
    rm /usr/share/applications/x11vnc.desktop & \
    rm /usr/share/applications/lxde-x-www-browser.desktop & \
    ln -s /usr/share/applications/firefox.desktop /usr/share/applications/lxde-x-www-browser.desktop & \
    rm /usr/share/applications/lxde-x-terminal-emulator.desktop  & \
    rm -rf /usr/share/ImageMagick-6



ENTRYPOINT ["/startup.sh"]
